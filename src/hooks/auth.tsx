import React, { createContext, useCallback, useState, useContext } from 'react';
import api from '../services/apiClient';

interface User {
  id: string;
  email: string;
}
interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  user: User;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
}

interface AuthState {
  token: string;
  client: string;
  uid: string;
  user: User;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>(() => {
    const token = localStorage.getItem('@EmpresasWeb:token');
    const uid = localStorage.getItem('@EmpresasWeb:uid');
    const client = localStorage.getItem('@EmpresasWeb:client');
    const user = localStorage.getItem('@EmpresasWeb:user');

    if (token && client && uid && user) {
      api.defaults.headers['access-token'] = token;
      api.defaults.headers.client = client;
      api.defaults.headers.uid = uid;

      return { token, client, uid, user: JSON.parse(user) };
    }

    return {} as AuthState;
  });

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.post('users/auth/sign_in', {
      email,
      password,
    });

    if (!response.data) {
      throw new Error();
    }

    const { id } = response.data.investor;
    const token = response.headers['access-token'];
    const { uid, client } = response.headers;
    const user: User = { id, email };

    localStorage.setItem('@EmpresasWeb:token', token);
    localStorage.setItem('@EmpresasWeb:uid', uid);
    localStorage.setItem('@EmpresasWeb:client', client);
    localStorage.setItem('@EmpresasWeb:user', JSON.stringify(user));

    api.defaults.headers['access-token'] = token;
    api.defaults.headers.client = client;
    api.defaults.headers.uid = uid;

    setData({ token, client, uid, user });
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('@EmpresasWeb:token');
    localStorage.removeItem('@EmpresasWeb:uid');
    localStorage.removeItem('@EmpresasWeb:client');
    localStorage.removeItem('@EmpresasWeb:user');

    setData({} as AuthState);
  }, []);

  return (
    <AuthContext.Provider value={{ user: data.user, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  return context;
}
