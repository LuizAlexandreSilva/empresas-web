import React, { useCallback, useEffect, useState } from 'react';
import {
  Card,
  CardMedia,
  Container,
  IconButton,
  Typography,
} from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';

import Header from '../../components/Header';
import useStyles from './styles';
import { useHistory, useParams } from 'react-router-dom';
import api from '../../services/apiClient';
import { IEnterprise } from '../Home';

interface IParams {
  id: string;
}

const EnterpriseDetail: React.FC = () => {
  const [enterprise, setEnterprise] = useState<IEnterprise>();
  const styles = useStyles();
  const history = useHistory();
  const params = useParams<IParams>();

  const handleBack = useCallback(() => {
    history.goBack();
  }, [history]);

  useEffect(() => {
    api
      .get(`enterprises/${params.id}`)
      .then((response) => setEnterprise(response.data.enterprise))
      .catch(() => history.push('/home'));
  }, [params.id]);

  return (
    <>
      <Header>
        <Container className={styles.headContainer}>
          <IconButton aria-label="back" onClick={handleBack}>
            <ArrowBack htmlColor="white" className={styles.icon} />
          </IconButton>
          <Typography className={styles.title} variant="h5">
            {enterprise?.enterprise_name}
          </Typography>
        </Container>
      </Header>
      <Container className={styles.body}>
        <Card className={styles.card}>
          <CardMedia
            className={styles.image}
            image={`https://empresas.ioasys.com.br/${enterprise?.photo}`}
          />
          <Typography color="textSecondary" className={styles.description}>
            {enterprise?.description}
          </Typography>
        </Card>
      </Container>
    </>
  );
};

export default EnterpriseDetail;
