import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  headContainer: {
    display: 'flex',
    alignItems: 'end',
    height: '100%',
    padding: '2rem',
  },
  title: {
    marginLeft: '2rem',
    paddingBottom: '15px',
  },
  body: {
    marginTop: '2.75rem',
    marginBottom: '2.75rem',
  },
  card: {
    padding: '3rem 4.5rem',
    backgroundColor: 'white',
    borderRadius: '4.8px',
  },
  image: {
    width: '100%',
    height: '18rem',
    marginBottom: '3rem',
    backgroundColor: 'gray',
  },
  description: {
    fontSize: '2.125rem',
  },
  icon: {
    height: '2.5rem',
    width: '2.5rem',
  },
});

export default useStyles;
