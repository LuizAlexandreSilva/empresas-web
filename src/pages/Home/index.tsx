import React, { useCallback, useState } from 'react';
import { Close, Search } from '@material-ui/icons';
import {
  Box,
  Button,
  Container,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import Header from '../../components/Header';
import logoWhite from '../../assets/logo-nav.png';
import useStyles from './styles';
import EnterpriseListItem from '../../components/EnterpriseListItem';
import api from '../../services/apiClient';
import Loading from '../../components/Loading';
import { useAuth } from '../../hooks/auth';

export interface IEnterprise {
  id: number;
  enterprise_name: string;
  description: string;
  photo: string;
  enterprise_type: {
    enterprise_type_name: string;
  };
  country: string;
}

const Home: React.FC = () => {
  const [showInput, setShowInput] = useState(false);
  const [searchString, setSearchString] = useState('');
  const [enterprises, setEnterprises] = useState<IEnterprise[]>([]);
  const [loading, setLoading] = useState(false);
  const [infoString, setInfoString] = useState('Clique na busca para iniciar.');
  const { signOut } = useAuth();

  const styles = useStyles();

  const handleShowInput = useCallback((show: boolean) => {
    setShowInput(show);
    if (show) {
      setInfoString('');
    } else {
      setInfoString('Clique na busca para iniciar.');
      setSearchString('');
    }
  }, []);

  const handleChangeInput = useCallback((event): any => {
    setSearchString(event.target.value);
  }, []);

  const handleSearch = useCallback(
    async (event) => {
      if (event.key !== 'Enter') {
        return;
      }
      setLoading(true);

      const response = await api.get('enterprises', {
        params: {
          name: searchString,
        },
      });
      if (!response.data.enterprises.length) {
        setInfoString('Nenhuma empresa foi encontrada para a busca realizada.');
      }
      setEnterprises(response.data.enterprises);
      setLoading(false);
    },
    [searchString],
  );

  return (
    <>
      {loading && <Loading />}
      <Header>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          height="100%"
        >
          {!showInput ? (
            <Container className={styles.head}>
              <Box position="absolute" left="1.5rem">
                <Button variant="text" color="inherit" onClick={signOut}>
                  Sair
                </Button>
              </Box>
              <img src={logoWhite} alt="ioasys" />
              <Box position="absolute" right="1.5rem">
                <IconButton
                  aria-label="search"
                  onClick={() => handleShowInput(true)}
                >
                  <Search htmlColor="white" className={styles.icon} />
                </IconButton>
              </Box>
            </Container>
          ) : (
            <Container>
              <FormControl fullWidth>
                <Input
                  disabled={loading}
                  placeholder="Pesquisar"
                  className={styles.input}
                  defaultValue={searchString}
                  onChange={handleChangeInput}
                  onKeyDown={handleSearch}
                  startAdornment={
                    <InputAdornment position="start">
                      <Search htmlColor="white" />
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="close search input"
                        onClick={() => handleShowInput(false)}
                      >
                        <Close htmlColor="white" />
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
            </Container>
          )}
        </Box>
      </Header>
      <Container className={styles.container}>
        {infoString && enterprises.length === 0 && (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            marginTop="15rem"
          >
            <Typography variant="h5" color="textSecondary">
              {infoString}
            </Typography>
          </Box>
        )}
        {enterprises.length > 0 &&
          enterprises.map((enterprise) => (
            <EnterpriseListItem key={enterprise.id} enterprise={enterprise} />
          ))}
      </Container>
    </>
  );
};

export default Home;
