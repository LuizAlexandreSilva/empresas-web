import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  head: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  input: {
    color: 'white',
  },
  container: {
    marginTop: '2.75rem',
  },
  icon: {
    height: '2.5rem',
    width: '2.5rem',
  },
});

export default useStyles;
