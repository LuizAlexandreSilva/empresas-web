import React, { useCallback, useState } from 'react';
import {
  Box,
  Button,
  Container,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import {
  EmailOutlined,
  LockOpenOutlined,
  Visibility,
  VisibilityOff,
} from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

import logo from '../../assets/logo.png';
import Loading from '../../components/Loading';
import useStyles from './styles';
import { useAuth } from '../../hooks/auth';

const Login: React.FC = () => {
  const styles = useStyles();
  const [values, setValues] = useState({
    email: '',
    password: '',
    showPassword: false,
    loading: false,
    error: false,
  });
  const { signIn } = useAuth();
  const history = useHistory();

  const handleChange = useCallback(
    (prop: string) => (event: any) => {
      setValues({ ...values, [prop]: event.target.value, error: false });
    },
    [values],
  );

  const handleClickShowPassword = useCallback(() => {
    setValues({ ...values, showPassword: !values.showPassword });
  }, [values]);

  const handleMouseDownPassword = useCallback((event: any) => {
    event.preventDefault();
  }, []);

  const handleSubmit = useCallback(async () => {
    setValues({ ...values, loading: true });
    await signIn({
      email: values.email,
      password: values.password,
    })
      .then(() => history.push('/home'))
      .catch(() => setValues({ ...values, error: true, loading: false }));
  }, [values, signIn, history]);

  return (
    <>
      {values.loading && <Loading />}
      <Container maxWidth="sm">
        <Box
          height="100vh"
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          <img src={logo} alt="ioasys" className={styles.logo} />
          <Box width="60%">
            <Typography variant="h6" align="center" className={styles.welcome}>
              BEM-VINDO AO
              <br />
              EMPRESAS
            </Typography>
            <Typography variant="body1" align="center">
              Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
              accumsan.
            </Typography>
            <Box className={styles.form}>
              <FormControl fullWidth className={styles.emailForm}>
                <Input
                  className={styles.input}
                  color="primary"
                  placeholder="E-mail"
                  value={values.email}
                  onChange={handleChange('email')}
                  startAdornment={
                    <InputAdornment position="start">
                      <EmailOutlined color="primary" />
                    </InputAdornment>
                  }
                />
              </FormControl>
              <FormControl fullWidth>
                <Input
                  placeholder="Password"
                  className={styles.input}
                  type={values.showPassword ? 'text' : 'password'}
                  value={values.password}
                  onChange={handleChange('password')}
                  startAdornment={
                    <InputAdornment position="start">
                      <LockOpenOutlined color="primary" />
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {values.showPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
              {values.error && (
                <Typography
                  color="error"
                  align="center"
                  className={styles.error}
                >
                  Credenciais informadas são inválidas, tente novamente.
                </Typography>
              )}
            </Box>

            <Button
              fullWidth
              variant="contained"
              color="secondary"
              className={styles.button}
              onClick={handleSubmit}
              disabled={!values.email.length || !values.password.length}
            >
              ENTRAR
            </Button>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default Login;
