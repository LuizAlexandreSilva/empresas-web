import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  logo: {
    width: '18.5rem',
    height: '4.5rem',
    objectFit: 'contain',
  },
  welcome: {
    fontSize: '1.6rem',
    fontWeight: 'bold',
    letterSpacing: '-1.2px',
    marginTop: '4rem',
    marginBottom: '1.5rem',
    lineHeight: 'normal',
  },
  form: {
    marginTop: '2.875rem',
    marginBottom: '2.5rem',
  },
  emailForm: {
    marginBottom: '2rem',
  },
  input: {
    fontSize: '1.35rem',
  },
  error: {
    fontSize: '0.75rem',
    marginTop: '0.8rem',
    marginBottom: '-2rem',
  },
  button: {
    fontSize: '1.2rem',
    fontWeight: 500,
  },
});

export default useStyles;
