import React, { useCallback } from 'react';
import { Box, Button, Container, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const NotFound: React.FC = () => {
  const history = useHistory();

  const handleClick = useCallback(() => {
    history.push('/home');
  }, [history]);

  return (
    <Container>
      <Box
        display="flex"
        flexDirection="column"
        height="100vh"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="h3" style={{ marginBottom: '2rem' }}>
          Are you lost?
        </Typography>
        <Button variant="contained" color="secondary" onClick={handleClick}>
          Go To Home
        </Button>
      </Box>
    </Container>
  );
};

export default NotFound;
