import React from 'react';
import { Switch } from 'react-router-dom';
import EnterpriseDetail from '../pages/EnterpriseDetail';
import Home from '../pages/Home';
import Login from '../pages/Login';
import NotFound from '../pages/NotFound';
import Route from './Route';

export default function Routes() {
  return (
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/home" component={Home} isPrivate />
      <Route
        path="/enterprise-detail/:id"
        component={EnterpriseDetail}
        isPrivate
      />

      <Route path="**" component={NotFound} />
    </Switch>
  );
}
