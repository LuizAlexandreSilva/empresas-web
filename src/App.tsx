import React from 'react';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import theme from './styles/theme';
import Routes from './routes';
import AppProvider from './hooks';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (
    <Router>
      <AppProvider>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Routes />
        </ThemeProvider>
      </AppProvider>
    </Router>
  );
}

export default App;
