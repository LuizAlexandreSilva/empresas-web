import React from 'react';
import { Box } from '@material-ui/core';
import useStyles from './styles';

const Header: React.FC = ({ children }) => {
  const styles = useStyles();

  return (
    <Box component="header" className={styles.head}>
      {children}
    </Box>
  );
};

export default Header;
