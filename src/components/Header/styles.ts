import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  head: {
    width: '100%',
    height: '9.5rem',
    backgroundImage: 'linear-gradient(180deg, #ee4c77 20%, #bd3c68 80%)',
    color: 'white',
  },
});

export default useStyles;
