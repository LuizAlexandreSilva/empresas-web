import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  container: {
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 900,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    width: '100%',
    backdropFilter: 'blur(1px)',
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
  },
});

export default useStyles;
