import React from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import useStyles from './styles';

const Loading: React.FC = () => {
  const styles = useStyles();

  return (
    <Box className={styles.container}>
      <CircularProgress color="secondary" size="8rem" />
    </Box>
  );
};

export default Loading;
