import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  card: {
    backgroundColor: 'white',
    borderRadius: '4px',
    padding: '1.4rem',
    width: '100%',

    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    marginBottom: '1.25rem',
  },
  image: {
    height: '10rem',
    width: '18rem',
    objectFit: 'contain',
    marginRight: '2.4rem',
    backgroundColor: 'gray',
  },
  name: {
    fontSize: '1.875rem',
    fontWeight: 'bold',
  },
  category: {
    fontSize: '1.5rem',
    color: '#8d8c8c',
  },
  country: {
    fontSize: '1.125rem',
    color: '#8d8c8c',
  },
});

export default useStyles;
