import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from '@material-ui/core';
import React, { HTMLAttributes, useCallback } from 'react';
import useStyles from './styles';

import { IEnterprise } from '../../pages/Home';
import { useHistory } from 'react-router-dom';

interface IProps extends HTMLAttributes<HTMLElement> {
  enterprise: IEnterprise;
}

const EnterpriseListItem: React.FC<IProps> = ({ enterprise }) => {
  const styles = useStyles();
  const history = useHistory();

  const handleClick = useCallback(
    (id: number) => {
      history.push(`enterprise-detail/${id}`);
    },
    [history],
  );

  return (
    <Card className={styles.card} onClick={() => handleClick(enterprise.id)}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={4} lg={3}>
          <CardMedia
            className={styles.image}
            image={`https://empresas.ioasys.com.br/${enterprise.photo}`}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={8} lg={9}>
          <CardContent>
            <Typography className={styles.name}>
              {enterprise.enterprise_name}
            </Typography>
            <Typography className={styles.category}>
              {enterprise.enterprise_type.enterprise_type_name}
            </Typography>
            <Typography className={styles.country}>
              {enterprise.country}
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  );
};

export default EnterpriseListItem;
