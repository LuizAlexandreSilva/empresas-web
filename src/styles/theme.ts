import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Roboto, sans-serif',
  },
  palette: {
    primary: {
      main: '#ee4c77',
    },
    error: {
      main: '#ff0f44',
    },
    secondary: {
      main: '#57bbbc',
      contrastText: '#fff',
    },
    text: {
      primary: '#383743',
      secondary: '#8d8c8c',
    },
    background: {
      default: '#eeecdb',
    },
  },
});

export default theme;
